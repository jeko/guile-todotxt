(define-module (todotxt)
  #:use-module (srfi srfi-9)
  #:export (todo.txt->scm
	    make-task
	    task-description
	    task-completion))

(define COMPLETION_MARK "x ")
(define COMPLETION_MARK_START_INDEX 0)
(define COMPLETION_MARK_LENGTH 2)
(define COMPLETED #t)
(define NOT_COMPLETED #f)

(define-record-type <task>
  (make-task description completion)
  task?
  (description task-description)
  (completion task-completion))

(define (todo.txt->scm line)
  (let ((task (make-task line NOT_COMPLETED)))
    (if (string-null? line)
	task
	(if (equal? COMPLETION_MARK
		    (substring line
			       COMPLETION_MARK_START_INDEX
			       (+ COMPLETION_MARK_START_INDEX COMPLETION_MARK_LENGTH)))
	    (make-task line COMPLETED)
	    task))))
