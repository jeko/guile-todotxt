(define-module (todotxt-test)
  #:use-module (srfi srfi-64)
  #:use-module (todotxt))

(module-define! (resolve-module '(srfi srfi-64)) 'test-log-to-file #f)

(define EMPTY_DESCRIPTION "")
(define DUMMY_DESCRIPTION "dummy-task")

(test-begin "Todo.txt-test-harness")

(test-equal "no-description"
  EMPTY_DESCRIPTION
  (task-description (todo.txt->scm EMPTY_DESCRIPTION)))

(test-equal "dummy-description"
  DUMMY_DESCRIPTION
  (task-description (todo.txt->scm DUMMY_DESCRIPTION)))

(test-equal "incomplete-task"
  #f
  (task-completion (todo.txt->scm "an incompleted task")))

(test-equal "completed-task"
  #t
  (task-completion (todo.txt->scm "x a completed task")))

(test-end "Todo.txt-test-harness")

